# -*- coding: utf-8 -*-
"""
Created on Sat Dec 16 15:48:10 2023

@author: User
"""

from tkinter import *
window = Tk()

def clicked():
    res = "Welcome to " + txt.get()
    lb22.configure(text= res)
    
window.title("Welcome to LikeGeeks app")
window.geometry('350x200')

# label
# lbl = Label(window, text="Hello", font=("Arial Bold", 50))
lbl = Label(window, text="Hello")
lbl.grid(column=0, row=0)

# button 
btn = Button(window, text="Click Me", bg="orange", fg="red", command=clicked)
btn.grid(column=1, row=0)

# label
lb21 = Label(window, text="Hello2")
lb21.grid(column=0, row=1)

# text
txt = Entry(window,width=10)
txt.grid(column=1, row=1)

# button 
btn = Button(window, text="Click Me", bg="orange", fg="red", command=clicked)
btn.grid(column=2, row=1)

# label
lb22 = Label(window, text="Hello2")
lb22.grid(column=3, row=1)


window.mainloop()